import axios from 'axios';

const API_URL = process.env.REACT_APP_NFT_API_URL;
const API_KEY = process.env.REACT_APP_NFT_API_KEY;

const API = axios.create({
  baseURL: API_URL,
  headers: {
    accept: 'application/json',
    'X-API-KEY': API_KEY,
  },
});

export default API;
