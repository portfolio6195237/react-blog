export const User = {
  getUser: 'GET_USER',
  getUserSuccess: 'GET_USER_SUCCESS',
  getUserError: 'GET_USER_ERROR',
  setUserAvatar: 'SET_USER_AVATAR',
  clearUserData: 'CLEAR_USER_DATA',
}
