import { User } from 'redux/constants/userConstant';

const initialState = {
  loading: false,
  error: null,
  data: {
    info: null,
    avatarURL: null,
  },
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case User.getUser:
      return { ...state, loading: true, error: null };
    case User.getUserSuccess:
      return {
        ...state,
        loading: false,
        data: { ...state.data, info: payload },
      };
    case User.getUserError:
      return { ...state, loading: false, error: payload };
    case User.setUserAvatar:
      return { ...state, data: { ...state.data, avatarURL: payload } };
    case User.clearUserData:
      return { ...state, data: { ...state.data, info: null } };
    default:
      return state;
  }
};
