/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useCallback } from 'react';
import Form from 'react-bootstrap/Form';
import { useDispatch, useSelector } from 'react-redux';
import { postsActions } from 'redux/actions';
import './SortBar.css';
const { sortPosts } = postsActions;

export default function SortBar() {
  const dispatch = useDispatch();
  const [sortOption, setSortOption] = useState('');
  const { filteredData } = useSelector(
    (state) => state.posts,
  );

  useEffect(() => {
    if (sortOption !== '') {
      dispatch(sortPosts(sortOption));
    }
  }, [filteredData]);

  const handleSelectChange = useCallback((event) => {
    setSortOption(event.target.value);
  }, []);

  const handleSortBtnClick = useCallback((event) => {
    event.preventDefault();
    dispatch(sortPosts(sortOption));
  }, [sortOption]);

  const handleResetBtnClick = useCallback(() => {
    setSortOption('');
    dispatch(sortPosts(''));
  }, []);

  return (
    <Form className="sort-bar">
      <Form.Group className="sort-bar__wrapper mb-3">
        <div>
          <Form.Select
            className="sort-bar__select"
            value={sortOption}
            name="option"
            onChange={handleSelectChange}
          >
            <option value="">Исходный</option>
            <option value="a-z">от A до Z</option>
            <option value="z-a">от Z до A</option>
          </Form.Select>
          <Form.Text className="sort-bar__select-hint text-muted">
            * порядок постов
          </Form.Text>
        </div>

        <button
          type="button"
          className="sort-bar__sort-btn btn btn-primary"
          onClick={handleSortBtnClick}
        >
          Применить
        </button>

        {sortOption && (
          <button
            type="button"
            className="sort-bar__reset-btn btn btn-danger"
            onClick={handleResetBtnClick}
          >
            Сброс
          </button>
        )}
      </Form.Group>
    </Form>
  );
}
