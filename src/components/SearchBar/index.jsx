import { useRef, useState } from 'react';
import Form from 'react-bootstrap/Form';
import { useDispatch } from 'react-redux';
import { postsActions } from 'redux/actions';
import './SearchBar.css';
const { filterPostsByTitle } = postsActions;

export default function SearchBar() {
  const dispatch = useDispatch();
  const [query, setQuery] = useState('');
  const inputRef = useRef(null);

  const handleSearch = (event) => {
    event.preventDefault();

    const trimmedQuery = query.trim();

    if (trimmedQuery.length) {
      dispatch(filterPostsByTitle(trimmedQuery));
    } else {
      setQuery('');
      inputRef.current.focus();
    }
  };

  const handleResetBtnClick = () => {
    setQuery('');
    dispatch(filterPostsByTitle(''));
  };

  const handleInputChange = ({ target }) => {
    const { value } = target;

    setQuery(value);

    if (value === '') {
      dispatch(filterPostsByTitle(''));
    }
  };

  return (
    <Form className="search-bar mb-4" onSubmit={handleSearch}>
      <Form.Group className="search-bar__group d-flex mb-3">
        <div className="search-bar__input-wrapper">
          <Form.Control
            type="text"
            className="search-bar__input"
            placeholder="Введите заголовок поста или его часть"
            value={query}
            onChange={handleInputChange}
            ref={inputRef}
          />

          {query && (
            <button
              type="button"
              className="search-bar__reset-btn btn btn-link text-decoration-none"
              onClick={handleResetBtnClick}
            >
              <span>X</span>
            </button>
          )}
        </div>

        <button
          type="submit"
          className="search-bar__search-btn btn btn-primary"
        >
          Поиск
        </button>
      </Form.Group>
    </Form>
  );
}
