import { ListGroup } from 'react-bootstrap';
import './CommentsList.css';
import { Scrollbar } from 'smooth-scrollbar-react';

export default function CommentsList({ data }) {
  if (!data) {
    return null;
  }

  return (
    <div className="card__comments mt-3">
      <Scrollbar
        alwaysShowTracks
        plugins={{
          overscroll: {
            effect: 'glow',
          },
        }}
      >
        <div className="scrollbar-container" style={{ maxHeight: 250 }}>
          {data.map(({ id, email, body }) => (
            <ListGroup.Item key={id} className="comment">
              <h6>Почта:</h6>
              <a href={`mailto:${email}`} target="_blank" rel="noreferrer">
                {email}
              </a>
              <h6 className="mt-2">Сообщение:</h6>
              <span>{body}</span>
            </ListGroup.Item>
          ))}
        </div>
      </Scrollbar>
    </div>
  );
}
