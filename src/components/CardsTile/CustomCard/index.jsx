import Avatar from 'components/Avatar/Avatar';
import CustomAlert from 'components/CustomAlert';
import Spinner from 'components/Spinner';
import ShowButton from './ShowNFTBtn';
import { Card, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { commentsActions, userActions } from 'redux/actions';
import CommentsList from './CommentsList';
import CommentsBtn from './CommentsBtn';
import './CustomCard.css';
const { getComments } = commentsActions;
const { setUserAvatar } = userActions;

export default function CustomCard({ id, title, body, userId, avatar }) {
  const dispatch = useDispatch();
  const { loading, data, error, postId } = useSelector(
    (state) => state.comments,
  );
  const commentsBtnTitle = 'Комментарии';

  let comments;

  if (id === postId) {
    if (loading) {
      comments = <Spinner />;
    } else if (error) {
      comments = <CustomAlert>{'Произошла ошибка: ' + error}</CustomAlert>;
    } else {
      comments = <CommentsList data={data} />;
    }
  }

  const handleAvatarClick = () => {
    dispatch(setUserAvatar(avatar));
  };

  const handleCommentsBtnClick = () => {
    dispatch(getComments(id));
  };

  return (
    <Col className="mb-4">
      <Card className="card">
        <div className="d-flex align-items-center justify-content-center pt-3">
          {userId &&
            <Link to={`/user/${userId}`} onClick={handleAvatarClick}>
              <Avatar className={'card__avatar'} avatar={avatar} />
            </Link>
          }
        </div>
        <Card.Body className="d-flex flex-column">
          <div className="card__content">
            <h5 className="card__content__header fst-italic">Заголовок поста: </h5>
            <Card.Title className="card__title fw-bold">{title}</Card.Title>
            <h5 className="card__content__header fst-italic">Аннотация: </h5>
            <Card.Text className="card__body">{body}</Card.Text>
          </div>
          {userId && <ShowButton avatarURL={avatar} />}
          <CommentsBtn name={commentsBtnTitle} clickHandler={handleCommentsBtnClick} />
          {comments}
        </Card.Body>
      </Card>
    </Col>
  );
}
