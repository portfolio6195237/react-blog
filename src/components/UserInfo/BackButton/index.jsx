import { useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import './BackButton.css';

export default function BackButton({ title }) {
  const navigate = useNavigate();

  const handleGoBack = () => {
    navigate(-1);
  };

  return (
    <Button className="back-button mt-4 w-100 btn-lg" onClick={handleGoBack}>
      {title}
    </Button>
  );
}
